#ifndef __OPTIONS_MENU_ITEM_H__
#define __OPTIONS_MENU_ITEM_H__

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/string.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/menu.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/spinctrl.h>
#include <wx/toolbar.h>
#include <wx/frame.h>
#include "windowwithchilds.h"

class OptionsMenu
	: public wxMenu,
	  public WindowWithChilds
{
public:
	OptionsMenu();
	virtual void loadChilds();

protected:
	wxMenuItem* m_Keyboard;
	wxMenuItem* m_Simulator;
};

#endif // __OPTIONS_MENU_ITEM_H__