#ifndef __MAIN_FRAME_PANEL_H__
#define __MAIN_FRAME_PANEL_H__

#include <wx/wx.h>
#include "controlpanel.h"
#include "graphicpanel.h"
#include "motionpanel.h"
#include "windowwithchilds.h"
#include "graphicpanelOgre.h"

class MainFrame;

class MainFramePanel
	: public wxPanel,
	  public WindowWithChilds
{
public:
	MainFramePanel( MainFrame* _mainFrame );
	virtual void loadChilds();

protected:
	ControlPanel* m_ControlPanel;
	GraphicPanel* m_GraphicPanel;
	MotionPanel*  m_MotionPanel;
	GraphicPanelOgre* m_OgrePanel;
};

#endif // __MAIN_FRAME_PANEL_H__