#ifndef __SIMULATION_TIMESTEP_MENU_H__
#define __SIMULATION_TIMESTEP_MENU_H__

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/string.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/menu.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/spinctrl.h>
#include <wx/toolbar.h>
#include <wx/frame.h>
#include "windowwithchilds.h"

class SimulationTimestepMenu
	: public wxMenu,
	  public WindowWithChilds
{
public:
	SimulationTimestepMenu();
	virtual void loadChilds();

protected:
	wxMenuItem* m_Decrease;
	wxMenuItem* m_Increase;
	wxMenuItem* m_Fix;
};

#endif // __SIMULATION_TIMESTEP_MENU_H__