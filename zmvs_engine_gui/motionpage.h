#ifndef __MOTION_PAGE_H__
#define __MOTION_PAGE_H__

#include <wx/wx.h>
#include "windowwithchilds.h"

class MotionPage
	: public wxPanel,
	  public WindowWithChilds
{
public:
	MotionPage( wxWindow* _parent );
	void loadChilds();
};

#endif // __MOTION_PAGE_H__