#ifndef __CONTROL_PANEL_H__
#define __CONTROL_PANEL_H__

#include "controlbookctrl.h"
#include "windowwithchilds.h"

class ControlPanel
	: public wxPanel,
	  public WindowWithChilds
{
public:
	ControlPanel( wxWindow* _parent );
	virtual void loadChilds();
	~ControlPanel();

protected:
	ControlBookCtrl* m_Book;
};

#endif // __CONTROL_PANEL_H__