#ifndef __MOTION_PANEL_H__
#define __MOTION_PANEL_H__

#include <wx/wx.h>
#include "windowwithchilds.h"

class MotionPanel
	: public wxPanel,
	  public WindowWithChilds
{
public:
	MotionPanel( wxWindow* _parent );
	void loadChilds();

private:
	wxComboBox* m_MotionCombo;
};

#endif // __MOTION_PANEL_H__