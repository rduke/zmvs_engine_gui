#include "projectdialog.h"

ProjectDialog::ProjectDialog()
    : wxDialog( NULL, -1, wxT( "New project" ), wxDefaultPosition, wxSize( 500, 400 ) )
{

  wxPanel *panel = new wxPanel(this, -1);

  wxBoxSizer *vbox = new wxBoxSizer(wxVERTICAL);
  wxBoxSizer *hbox = new wxBoxSizer(wxHORIZONTAL);

  wxStaticBox *st = new wxStaticBox(panel, -1, wxT("Common"), 
      wxPoint(5, 5), wxSize(240, 150));
  wxTextCtrl* projectName = new wxTextCtrl(panel, wxID_ANY, wxT( "projectName" ), wxPoint(15, 30));

  wxButton *okButton = new wxButton(this, -1, wxT("Ok"), 
      wxDefaultPosition, wxSize(70, 30));
  wxButton *closeButton = new wxButton(this, -1, wxT("Close"), 
      wxDefaultPosition, wxSize(70, 30));

  hbox->Add(okButton, 1);
  hbox->Add(closeButton, 1, wxLEFT, 5);

  vbox->Add(panel, 1);
  vbox->Add(hbox, 0, wxALIGN_CENTER | wxTOP | wxBOTTOM, 10);

  SetSizer(vbox);

  Centre();
  ShowModal();

  Destroy(); 
}