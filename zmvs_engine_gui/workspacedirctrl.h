#ifndef __WORKSPACE_DIR_CTRL_H__
#define __WORKSPACE_DIR_CTRL_H__

#include <wx/frame.h>
#include <wx/dirctrl.h>

class WorkspaceDirCtrl
	: public wxGenericDirCtrl
{
public:
	WorkspaceDirCtrl( wxWindow* _parent );
};

#endif // __WORKSPACE_DIR_CTRL_H__