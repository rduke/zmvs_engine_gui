#ifndef __GRAPHIC_PANEL_H__
#define __GRAPHIC_PANEL_H__

#include "openglframe.h"
#include "windowwithchilds.h"

class GraphicPanel
	: public wxPanel,
	  public WindowWithChilds
{
public:
	GraphicPanel( wxWindow* _parent );
	virtual void loadChilds();

protected:
    OpenGLFrame* m_OpenGLFrame;
};

#endif // __GRAPHIC_PANEL_H__