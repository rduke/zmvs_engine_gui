#include "filemenu.h"

FileMenu::FileMenu()
: m_New( NULL ),
  m_Open( NULL ),
  m_Save( NULL ),
  m_SaveAs( NULL ),
  m_Exit( NULL )
{

}

void FileMenu::loadChilds()
{
	m_New = new wxMenuItem( this,
		                    wxID_ANY,
							wxString( wxT("New") ) + wxT('\t') + wxT("N"),
							wxEmptyString,
							wxITEM_NORMAL );

	Append( m_New );
	
	m_Open = new wxMenuItem( this,
		                     wxID_ANY,
							 wxString( wxT("Open") ) + wxT('\t') + wxT("O"),
							 wxEmptyString,
							 wxITEM_NORMAL );

	Append( m_Open );
	
	m_Save = new wxMenuItem( this,
		                     wxID_ANY,
							 wxString( wxT("Save") ) + wxT('\t') + wxT("S"),
							 wxEmptyString,
							 wxITEM_NORMAL );

	Append( m_Save );
	
	m_SaveAs = new wxMenuItem( this,
		                       wxID_ANY,
							   wxString( wxT("Save as") ) + wxT('\t') + wxT("V"),
							   wxEmptyString,
							   wxITEM_NORMAL );

	Append( m_SaveAs );

	m_Exit = new wxMenuItem( this,
		                     wxID_ANY,
							 wxString( wxT("Exit") ) + wxT('\t') + wxT("E"),
							 wxEmptyString,
							 wxITEM_NORMAL );

	Append( m_Exit );
}