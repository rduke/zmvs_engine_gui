#include "controlbookctrl.h"

ControlBookCtrl::ControlBookCtrl( wxWindow* _parent )
: wxBookCtrl( _parent, wxID_ANY, wxPoint(0, 0), wxSize(795, 175) ),
  m_Steering( NULL ),
  m_Motion( NULL )
{
}

void ControlBookCtrl::loadChilds()
{
	m_Steering = new SteeringPage( this );
	AddPage( m_Steering, _T("Steering"), false );

	m_Motion = new MotionPage( this );
	AddPage( m_Motion, _T("Motion"), false );
}