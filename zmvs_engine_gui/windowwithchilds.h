#ifndef __WINDOW_H__
#define __WINDOW_H__

class WindowWithChilds
{
public:
	virtual ~WindowWithChilds() { }
	virtual void loadChilds() = 0;
};

#endif // __WINDOW_H__