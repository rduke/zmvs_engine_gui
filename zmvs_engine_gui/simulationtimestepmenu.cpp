#include "simulationtimestepmenu.h"

SimulationTimestepMenu::SimulationTimestepMenu()
: m_Decrease( NULL ),
  m_Increase( NULL ),
  m_Fix( NULL )
{
}

void SimulationTimestepMenu::loadChilds()
{
	m_Decrease = new wxMenuItem( this,
		                         wxID_ANY, wxString( wxT("Decrease") ) + wxT('\t') + wxT("D"),
								 wxEmptyString,
								 wxITEM_NORMAL );

    Append( m_Decrease );
	
	m_Increase = new wxMenuItem( this,
		                         wxID_ANY,
								 wxString( wxT("Increase") ) + wxT('\t') + wxT("I"),
								 wxEmptyString,
								 wxITEM_NORMAL );

	Append( m_Increase );

	m_Fix = new wxMenuItem( this, 
		                    wxID_ANY,
							wxString( wxT("Fix") ) + wxT('\t') + wxT("F"),
		                    wxEmptyString,
							wxITEM_NORMAL );

    Append( m_Fix );
}