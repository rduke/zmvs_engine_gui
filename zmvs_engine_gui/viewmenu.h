#ifndef __VIEW_MENU_H__
#define __VIEW_MENU_H__

#include <wx/menu.h>
#include "windowwithchilds.h"

class ViewMenu
	: public wxMenu,
	  public WindowWithChilds
{
public:
	ViewMenu();
	virtual void loadChilds();

protected:
    // TODO
};

#endif // __VIEW_MENU_H__