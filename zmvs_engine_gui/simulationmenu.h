#ifndef __SIMULATION_MENU_H__
#define __SIMULATION_MENU_H__

#include "simulationtimestepmenu.h"

class SimulationMenu
	: public wxMenu
{
public:
	SimulationMenu();
	virtual void loadChilds();

protected:
	SimulationTimestepMenu* m_Timestep;
	wxMenuItem*             m_Run;
    wxMenuItem*             m_Pause;
	wxMenuItem*             m_Stop;
};

#endif // __SIMULATION_MENU_H__