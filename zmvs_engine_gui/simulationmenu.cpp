#include "simulationmenu.h"

SimulationMenu::SimulationMenu()
: m_Timestep( NULL ),
  m_Run( NULL ),
  m_Pause( NULL ),
  m_Stop( NULL )
{
}

void SimulationMenu::loadChilds()
{
	m_Timestep = new SimulationTimestepMenu();

	m_Timestep->loadChilds();

	Append( wxID_ANY, wxT("timestep"), m_Timestep );

	m_Run = new wxMenuItem( this,
		                    wxID_ANY,
							wxString( wxT("Run") ) + wxT('\t') + wxT("R"),
							wxEmptyString,
							wxITEM_NORMAL );

	Append( m_Run );

	m_Pause = new wxMenuItem( this,
		                      wxID_ANY,
							  wxString( wxT("Pause") ) + wxT('\t') + wxT("P"),
							  wxEmptyString,
							  wxITEM_NORMAL );

	Append( m_Pause );
	
	m_Stop = new wxMenuItem( this,
		                     wxID_ANY, wxString( wxT("Stop") ) + wxT('\t') + wxT("S"),
							 wxEmptyString,
							 wxITEM_NORMAL );

	Append( m_Stop );
}
