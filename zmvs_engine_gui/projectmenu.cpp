#include "projectmenu.h"

ProjectMenu::ProjectMenu()
: m_Build( NULL ),
  m_Rebuild( NULL ),
  m_Clean( NULL )
{
}

void ProjectMenu::loadChilds()
{
	m_Build = new wxMenuItem( this,
		                      wxID_ANY, wxString( wxT("Build") ) + wxT('\t') + wxT("B"),
							  wxEmptyString,
							  wxITEM_NORMAL );

	Append( m_Build );
	
	m_Rebuild = new wxMenuItem( this,
		                        wxID_ANY, wxString( wxT("Rebuild") ) + wxT('\t') + wxT("R"),
								wxEmptyString,
								wxITEM_NORMAL );

	Append( m_Rebuild );
	
	m_Clean = new wxMenuItem( this,
		                      wxID_ANY,
							  wxString( wxT("Clean") ) + wxT('\t') + wxT("C"),
							  wxEmptyString,
							  wxITEM_NORMAL );

	Append( m_Clean );
}

