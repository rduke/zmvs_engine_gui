#include "main.h"
#include "mainframe.h"
#include <Ogre.h>

IMPLEMENT_APP(MyApp)

bool MyApp::OnInit()
{
	Ogre::Root* root = new Ogre::Root( "plugins_d.cfg" );


	// Load resource paths from config file
	Ogre::ConfigFile cf;
	cf.load( "resources_d.cfg" );

	// Go through all sections & settings in the file
	Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();

	Ogre::String secName, typeName, archName;

	while( seci.hasMoreElements() )
	{
		secName = seci.peekNextKey();
		Ogre::ConfigFile::SettingsMultiMap *settings = seci.getNext();
		Ogre::ConfigFile::SettingsMultiMap::iterator i;
		for ( i = settings->begin(); i != settings->end(); ++i )
		{
			typeName = i->first;
			archName = i->second;
			Ogre::ResourceGroupManager::getSingleton().addResourceLocation(
				archName
				,	typeName
				,	secName
				);
		}
	}
    MainFrame* menu = new MainFrame(NULL);
    menu->Show(true);

    return true;
}