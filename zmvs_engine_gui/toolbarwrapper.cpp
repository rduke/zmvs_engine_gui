#include "toolbarwrapper.h"

ToolBarWrapper::ToolBarWrapper( wxToolBar *_toolBar, wxFrame* _parentFrame )
: m_ToolBar( _toolBar ),
  m_ParentFrame( _parentFrame )
{

}

void ToolBarWrapper::loadChilds()
{	
	// Just for test
	wxObject* tool = NULL;
	tool = m_ToolBar->AddTool( 
		                wxID_NEW,
		                wxT("new"),
						wxBitmap( wxT("../resources/icons/folder_add_16.png"), wxBITMAP_TYPE_ANY ),
						wxNullBitmap,
						wxITEM_NORMAL,
						wxEmptyString, 
						wxEmptyString,
						NULL ); 
	
	tool = m_ToolBar->AddTool(
		                wxID_OPEN,
		                wxT("openTool"),
						wxBitmap( wxT("../resources/icons/open_16.png"), wxBITMAP_TYPE_ANY ),
						wxNullBitmap,
						wxITEM_NORMAL,
						wxEmptyString,
						wxEmptyString,
						NULL );
	
	tool = m_ToolBar->AddTool( 
		                wxID_SAVE,
		                wxT("saveTool"),
						wxBitmap( wxT("../resources/icons/box_16.png"), wxBITMAP_TYPE_ANY ),
						wxNullBitmap,
						wxITEM_NORMAL,
						wxEmptyString,
						wxEmptyString,
						NULL );
	
	m_ToolBar->AddSeparator(); 
	
	tool = m_ToolBar->AddTool( 
		                wxID_APPLY,
		                wxT("buildTool"),
						wxBitmap( wxT("../resources/icons/run-build-2.png"), wxBITMAP_TYPE_ANY ),
						wxNullBitmap,
						wxITEM_NORMAL,
						wxEmptyString,
						wxEmptyString,
						NULL );
	
	tool = m_ToolBar->AddTool( 
		                wxID_ANY,
		                wxT("buildRunTool"),
						wxBitmap( wxT("../resources/icons/run-build-install.png"), wxBITMAP_TYPE_ANY ),
						wxNullBitmap,
						wxITEM_NORMAL,
						wxEmptyString,
						wxEmptyString,
						NULL ); 
	
	m_ToolBar->AddSeparator(); 
	
	tool = m_ToolBar->AddTool( 
		               wxID_ANY,
		               wxT("Run"), 
					   wxBitmap( wxT("../resources/icons/media-playback-start-5.png"), wxBITMAP_TYPE_ANY ),
					   wxNullBitmap,
					   wxITEM_NORMAL,
					   wxEmptyString,
					   wxEmptyString,
					   NULL );
	
	tool = m_ToolBar->AddTool( 
		                wxID_ANY,
		                wxT("pause"),
						wxBitmap( wxT("../resources/icons/media-playback-pause-5.png"), wxBITMAP_TYPE_ANY ),
						wxNullBitmap,
						wxITEM_NORMAL,
						wxEmptyString,
						wxEmptyString,
						NULL );
	
	tool = m_ToolBar->AddTool( 
		                wxID_ANY,
		                wxT("stop"),
						wxBitmap( wxT("../resources/icons/media-playback-stop-5.png"), wxBITMAP_TYPE_ANY ),
						wxNullBitmap,
						wxITEM_NORMAL,
						wxEmptyString,
						wxEmptyString,
						NULL ); 
	
	tool = new wxSpinCtrl( 
		                             m_ToolBar,
		                             wxID_ANY, wxT("60"),
									 wxDefaultPosition,
									 wxDefaultSize,
									 wxSP_WRAP,
									 20,
									 100,
									 60 );

	m_ToolBar->AddControl( static_cast< wxSpinCtrl* >( tool ) );
	m_ToolBar->Realize(); 
    m_ToolBar->Connect(wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( ToolBarWrapper::onToolBarItemClick ) );
}

void ToolBarWrapper::onToolBarItemClick(wxCommandEvent& event)
{
	wxString windowId = wxString() << event.GetId();
	m_ParentFrame->SetTitle( windowId );
	if( event.GetId() == wxID_NEW )
		onNewProjectClick( event );
	else if( event.GetId() == wxID_SAVE )
		onSaveProjectClick( event );
}

void ToolBarWrapper::onNewProjectClick(wxCommandEvent& event)
{
	ProjectDialog newProjectDialog;
}

void ToolBarWrapper::onOpenProjectClick(wxCommandEvent& event)
{
   wxFileDialog 
        openFileDialog(m_ParentFrame, _("Open XYZ file"), "", "",
                       "XYZ files (*.xyz)|*.xyz", wxFD_OPEN|wxFD_FILE_MUST_EXIST);

    if (openFileDialog.ShowModal() == wxID_CANCEL)
        return;     // the user changed idea...
    
    // proceed loading the file chosen by the user;
    // this can be done with e.g. wxWidgets input streams:
    wxFileInputStream input_stream(openFileDialog.GetPath());
    if (!input_stream.IsOk())
    {
        wxLogError("Cannot open file '%s'.", openFileDialog.GetPath());
        return;
    }
}

void ToolBarWrapper::onSaveProjectClick(wxCommandEvent& event)
{
	 wxFileDialog 
        saveFileDialog(m_ParentFrame, _("Save XYZ file"), "", "",
                       "XYZ files (*.xyz)|*.xyz", wxFD_SAVE|wxFD_OVERWRITE_PROMPT);

    if (saveFileDialog.ShowModal() == wxID_CANCEL)
        return;     // the user changed idea...
    
    // save the current contents in the file;
    // this can be done with e.g. wxWidgets output streams:
    wxFileOutputStream output_stream(saveFileDialog.GetPath());
    if (!output_stream.IsOk())
    {
        wxLogError("Cannot save current contents in file '%s'.", saveFileDialog.GetPath());
        return;
    }
}

void ToolBarWrapper::onBuildProjectClick(wxCommandEvent& event)
{
}

void ToolBarWrapper::onBuildRunProjectClick(wxCommandEvent& event)
{
}

void ToolBarWrapper::onSimulationRunClick(wxCommandEvent& event)
{
}

void ToolBarWrapper::onSimulationPauseClick(wxCommandEvent& event)
{
}

void ToolBarWrapper::onSimulationStopClick(wxCommandEvent& event)
{
}
