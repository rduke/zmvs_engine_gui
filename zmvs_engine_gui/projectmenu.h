#ifndef __PROJECT_MENU_H__
#define __PROJECT_MENU_H__

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/string.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/menu.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/spinctrl.h>
#include <wx/toolbar.h>
#include <wx/frame.h>
#include "windowwithchilds.h"

class ProjectMenu
	: public wxMenu,
	  public WindowWithChilds
{
public:
	ProjectMenu();
    virtual void loadChilds();

protected:
	wxMenuItem* m_Build;
	wxMenuItem* m_Rebuild;
	wxMenuItem* m_Clean;
};

#endif // __PROJECT_MENU_H__