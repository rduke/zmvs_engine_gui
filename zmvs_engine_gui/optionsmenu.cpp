#include "optionsmenu.h"

OptionsMenu::OptionsMenu()
 : m_Keyboard( NULL ),
   m_Simulator( NULL )
{
}

void OptionsMenu::loadChilds()
{
	m_Keyboard = new wxMenuItem( this,
		                         wxID_ANY,
								 wxString( wxT("Keybard") ) + wxT('\t') + wxT("K"),
								 wxEmptyString,
								 wxITEM_NORMAL );

	Append( m_Keyboard );
	
	m_Simulator = new wxMenuItem( this,
		                          wxID_ANY,
								  wxString( wxT("Simulator") ) + wxT('\t') + wxT("S"),
								  wxEmptyString,
								  wxITEM_NORMAL );

	Append( m_Simulator );
}

