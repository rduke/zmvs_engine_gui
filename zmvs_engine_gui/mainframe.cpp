#include "mainframe.h"

MainFrame::MainFrame( wxWindow* parent,
					  wxWindowID id,
					  const wxString& title,
					  const wxPoint& pos,
					  const wxSize& size,
					  long style )
					  : wxFrame( parent, id, title, pos, size, style ),
					    m_ToolBarWrapper( NULL )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	m_AuiManager.SetManagedWindow(this);

	wxImage::AddHandler( new wxPNGHandler );

	m_MenuBar = new MenuBar( 0 );
	m_MenuBar->loadChilds();

    m_AuiManager.Update();

	m_Panel = new MainFramePanel( this );
	m_Panel->SetBackgroundColour( wxColour( wxT("blue") ) );
	m_Panel->loadChilds();

	/*m_AuiManager.AddPane(CreateNotebook(), wxAuiPaneInfo().Name(wxT("notebook_content")).
                  CenterPane().PaneBorder(false));*/

	//wxPanel* panel2 = new wxPanel( m_Panel, wxID_ANY, wxPoint(0, 0), wxSize(200, 400));//, wxEXPAND );
	//panel2->SetBackgroundColour( wxColor("green") );

	//wxPanel* panel = new wxPanel( m_Panel, wxID_ANY, wxPoint(0, 400), wxSize(800, 150));//, wxEXPAND );
	//panel->SetBackgroundColour( wxColor("yellow") );

	//wxPanel* panel3 = new wxPanel( m_Panel, wxID_ANY, wxPoint(200, 0), wxSize(600, 400));//, wxEXPAND );
	//panel3->SetBackgroundColour( wxColor("red") );

	this->SetMenuBar( m_MenuBar );

	m_ToolBarWrapper.reset( new ToolBarWrapper( this->CreateToolBar( wxTB_HORIZONTAL, wxID_ANY ), this ) );
	m_ToolBarWrapper->loadChilds();

	this->Centre( wxBOTH );
	this->SetMinSize( wxSize( 800, 600 ) );
	this->SetMaxSize( wxSize( 800, 600 ) );
}

MainFrame::~MainFrame()
{
	m_AuiManager.UnInit();
}

wxTreeCtrl* MainFrame::CreateTreeCtrl()
{
    wxTreeCtrl* tree = new wxTreeCtrl(this, wxID_ANY,
                                      wxPoint(0,0), wxSize(160,250),
                                      wxTR_DEFAULT_STYLE | wxNO_BORDER);

    wxImageList* imglist = new wxImageList(16, 16, true, 2);
    imglist->Add(wxArtProvider::GetBitmap(wxART_FOLDER, wxART_OTHER, wxSize(16,16)));
    imglist->Add(wxArtProvider::GetBitmap(wxART_NORMAL_FILE, wxART_OTHER, wxSize(16,16)));
    tree->AssignImageList(imglist);

    wxTreeItemId root = tree->AddRoot(wxT("wxAUI Project"), 0);
    wxArrayTreeItemIds items;

    items.Add(tree->AppendItem(root, wxT("Item 1"), 0));
    items.Add(tree->AppendItem(root, wxT("Item 2"), 0));
    items.Add(tree->AppendItem(root, wxT("Item 3"), 0));
    items.Add(tree->AppendItem(root, wxT("Item 4"), 0));
    items.Add(tree->AppendItem(root, wxT("Item 5"), 0));


    int i, count;
    for (i = 0, count = items.Count(); i < count; ++i)
    {
        wxTreeItemId id = items.Item(i);
        tree->AppendItem(id, wxT("Subitem 1"), 1);
        tree->AppendItem(id, wxT("Subitem 2"), 1);
        tree->AppendItem(id, wxT("Subitem 3"), 1);
        tree->AppendItem(id, wxT("Subitem 4"), 1);
        tree->AppendItem(id, wxT("Subitem 5"), 1);
    }


    tree->Expand(root);

    return tree;
}

wxAuiNotebook* MainFrame::CreateNotebook()
{
   wxPanel* panel = new wxPanel( m_Panel, wxID_ANY, wxPoint(0, 400), wxSize(800, 200), wxBORDER );
   panel->SetBackgroundColour( wxColor("yellow") );
    wxAuiNotebook* ctrl = new wxAuiNotebook(panel, wxID_ANY,
                                    wxPoint(0, 400),
                                    wxSize(800,200),
                                    wxAUI_NB_DEFAULT_STYLE | wxAUI_NB_TAB_EXTERNAL_MOVE | wxNO_BORDER);
   wxBoxSizer* horizSizer = new wxBoxSizer(wxHORIZONTAL);
   wxBoxSizer* vertSizer = new wxBoxSizer(wxVERTICAL);
   vertSizer->Add(horizSizer, 1);
	
   //horizSizer->Add( ctrl, 1 );
   panel->SetSizer( horizSizer );


   return ctrl;
}