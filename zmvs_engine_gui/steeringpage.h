#ifndef __STEERING_PAGE_H__
#define __STEERING_PAGE_H__

#include <wx/wx.h>
#include "windowwithchilds.h"

class SteeringPage
	: public wxPanel,
	  public WindowWithChilds
{
public:
	SteeringPage( wxWindow* _parent );
	virtual void loadChilds();
};

#endif // __STEERING_PAGE_H__