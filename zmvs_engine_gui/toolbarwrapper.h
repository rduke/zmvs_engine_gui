#ifndef __TOOL_BAR_WRAPPER_H__
#define __TOOL_BAR_WRAPPER_H__

#include <wx/spinctrl.h>
#include <wx/toolbar.h>
#include <wx/frame.h>
#include <wx/msgdlg.h>
#include <wx/log.h>
#include <wx/filedlg.h>
#include <wx/wfstream.h>
#include "projectdialog.h"
#include "windowwithchilds.h"

class ToolBarWrapper
	: public wxEvtHandler,
	  public WindowWithChilds
{
public:
	ToolBarWrapper( wxToolBar* _toolBar, wxFrame* _parentFrame );

	virtual void loadChilds();

	void onNewProjectClick(wxCommandEvent& event);
	void onOpenProjectClick(wxCommandEvent& event);
	void onSaveProjectClick(wxCommandEvent& event);
	void onBuildProjectClick(wxCommandEvent& event);
	void onBuildRunProjectClick(wxCommandEvent& event);
	void onSimulationRunClick(wxCommandEvent& event);
	void onSimulationPauseClick(wxCommandEvent& event);
	void onSimulationStopClick(wxCommandEvent& event);
	void onToolBarItemClick(wxCommandEvent& event);

protected:
    wxToolBar*         m_ToolBar;
    wxFrame*           m_ParentFrame;
};

#endif // __TOOL_BAR_WRAPPER_H__