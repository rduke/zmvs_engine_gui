#ifndef __GRAPHIC_PANEL_OGRE_H__
#define __GRAPHIC_PANEL_OGRE_H__

#include "windowwithchilds.h"
#include "wxOgreScene.h"
#include "wxOgreView.h"


class GraphicPanelOgre
	: public wxPanel,
	public WindowWithChilds
{
public:
	GraphicPanelOgre( wxWindow* _parent );
	virtual void loadChilds();
	virtual ~GraphicPanelOgre();
protected:
	///The main Ogre Scene
	wxOgreScene* mpMainScene;
	///The wxOgreView object
	wxOgreView* mpMainOgreView;
};

#endif // __GRAPHIC_PANEL_H__