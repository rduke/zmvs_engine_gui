#ifndef __MAIN_FRAME_H__
#define __MAIN_FRAME_H__

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/string.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/menu.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/spinctrl.h>
#include <wx/toolbar.h>
#include <wx/frame.h>
#include <wx/grid.h>
#include "wx/wxhtml.h"

#include "MenuBar.h"
#include "ToolBarWrapper.h"
#include "workspacedirctrl.h"
#include "mainframepanel.h"
#include <wx/aui/aui.h>
#include <memory>

class MainFrame 
	: public wxFrame 
{
public:
	MainFrame( wxWindow* parent,
		       wxWindowID id = wxID_ANY,
			   const wxString& title = wxT("zmvs engine gui"),
			   const wxPoint& pos = wxDefaultPosition,
			   const wxSize& size = wxSize( 800, 600 ),
			   long style = wxDEFAULT_FRAME_STYLE | wxTAB_TRAVERSAL );

	wxTreeCtrl* CreateTreeCtrl();
	wxAuiNotebook* CreateNotebook();
	~MainFrame();

private:

protected:
	MenuBar*                           m_MenuBar;
	std::auto_ptr< ToolBarWrapper >    m_ToolBarWrapper;
	MainFramePanel*                    m_Panel;
	wxBoxSizer*                        m_HrzSizer;
	wxBoxSizer*                        m_VrtSizer;
	wxAuiManager                       m_AuiManager;

};

#endif // __MAIN_FRAME_H__
