#ifndef __MENU_BAR_H__
#define __MENU_BAR_H__

#include "filemenu.h"
#include "viewmenu.h"
#include "helpmenu.h"
#include "projectmenu.h"
#include "optionsmenu.h"
#include "simulationmenu.h"
#include "windowwithchilds.h"

class MenuBar
	: public wxMenuBar,
	  public WindowWithChilds
{
public:
	MenuBar( long _style );
	virtual void loadChilds();

protected:
	FileMenu*       m_File;
	ViewMenu*       m_View;
	ProjectMenu*    m_Project;
	SimulationMenu* m_Simulation;
	HelpMenu*       m_Help;
};

#endif // __MENU_BAR_H__