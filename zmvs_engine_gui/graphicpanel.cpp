#include "graphicpanel.h"

GraphicPanel::GraphicPanel( wxWindow* _parent )
: wxPanel( _parent, wxID_ANY, wxPoint(200, 0), wxSize(600, 350) ),
  m_OpenGLFrame( NULL )
{
}

void GraphicPanel::loadChilds()
{
	m_OpenGLFrame = new OpenGLFrame( this );
}