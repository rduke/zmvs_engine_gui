#ifndef __FILE_MENU_H__
#define __FILE_MENU_H__

#include <wx/artprov.h>
#include <wx/string.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/menu.h>
#include <wx/settings.h>
#include <wx/frame.h>
#include "windowwithchilds.h"

class FileMenu
	: public wxMenu,
	  public WindowWithChilds
{
public:
	FileMenu();
	virtual void loadChilds();

protected:
	wxMenuItem* m_New;
	wxMenuItem* m_Open;
	wxMenuItem* m_Save;
	wxMenuItem* m_SaveAs;
	wxMenuItem* m_Exit;
};

#endif // __FILE_MENU_H__