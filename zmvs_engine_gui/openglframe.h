#ifndef __OPEN_GL_FRAME_H__
#define __OPEN_GL_FRAME_H__

#include "wx/glcanvas.h"
#include "wx/wxprec.h"
#include "windowwithchilds.h"

class OpenGLFrame
	: public wxGLCanvas/*,
	  public WindowWithChilds*/
{
public:
    OpenGLFrame( wxWindow* _parent );
	void OnPaint(wxPaintEvent& event);
	void OnEraseBackground(wxEraseEvent& WXUNUSED(event));
	void Render();
	void initGL();

	//DECLARE_EVENT_TABLE()
protected:
	bool m_init;
	int m_gllist;
};

#endif //  __OPEN_GL_FRAME_H__