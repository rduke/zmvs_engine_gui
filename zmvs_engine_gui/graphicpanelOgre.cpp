#include "graphicpanelogre.h"
#include "RenderSystems\GL\OgreGLRenderSystem.h"

GraphicPanelOgre::GraphicPanelOgre( wxWindow* _parent )
: wxPanel( _parent, wxID_ANY, /*wxT("Ogre view"),*/ wxPoint(200, 0), wxSize(600, 350) )
{
	//Attach menu items and the statusbar
	//Create(_parent, wxID_ANY, wxT("Ogre view"), wxPoint(200, 0), wxSize(600, 350), wxDEFAULT_FRAME_STYLE, _T("id"));

	//Create(_parent, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxDEFAULT_FRAME_STYLE, _T("id"));

	//Create the main Ogre scene
	//wxGridSizer* MainBoxSizer = new wxGridSizer(2,2,0,0);
	//this->SetSizer(MainBoxSizer);
	//MainBoxSizer->RecalcSizes();

	mpMainOgreView = new wxOgreView(this, wxID_ANY, wxT("Ogre view"), wxPoint(0, 0), wxSize(600, 350));
	mpMainOgreView->SetBackgroundColour("red");

	//MainBoxSizer->Add(mpMainOgreView,0, wxSHAPED|wxALL|wxALIGN_CENTER,5);
	//MainBoxSizer->Add(mpSecOgreView,0, wxSHAPED|wxALL|wxALIGN_CENTER,5);
	//MainBoxSizer->SetSizeHints( this );

	//this->Layout();

	mpMainScene = new wxOgreScene(mpMainOgreView);
    //SetSize(600,350);
}

void GraphicPanelOgre::loadChilds()
{

}


GraphicPanelOgre::~GraphicPanelOgre()
{
	//delete m_wxOgre;
}