#include "menubar.h"

MenuBar::MenuBar( long _style )
: wxMenuBar( _style )
{
}

void MenuBar::loadChilds()
{
    m_File = new FileMenu;
	m_View = new ViewMenu;
	m_Project = new ProjectMenu;
	m_Simulation = new SimulationMenu;
	m_Help = new HelpMenu;

	m_File->loadChilds();
	m_View->loadChilds();
	m_Project->loadChilds();
	m_Simulation->loadChilds();
	m_Help->loadChilds();

	Append( m_File, wxT("File") );
	Append( m_View, wxT("View") );
	Append( m_Project, wxT("Project") ); 
	Append( static_cast< wxMenu* >( m_Simulation ), wxT("Simulation") ); 
	Append( m_Help, wxT("Help") ); 
}