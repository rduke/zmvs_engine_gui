#ifndef __CONTROL_BOOK_CTRL_H__
#define __CONTROL_BOOK_CTRL_H__

#include <wx/bookctrl.h>
#include "windowwithchilds.h"
#include "steeringpage.h"
#include "motionpage.h"

class ControlBookCtrl
	: public wxBookCtrl,
	  public WindowWithChilds
{
public:
	ControlBookCtrl( wxWindow* _parent );
	virtual void loadChilds();

private:
	SteeringPage* m_Steering;
	MotionPage*   m_Motion;
};

#endif // __CONTROL_BOOK_CTRL_H__