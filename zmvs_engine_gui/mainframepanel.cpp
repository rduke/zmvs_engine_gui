#include "mainframepanel.h"
//#include <wx/app.h>

MainFramePanel::MainFramePanel( MainFrame *_mainFrame )
: wxPanel( (wxFrame*)_mainFrame, wxID_ANY ),
  m_ControlPanel( NULL ),
  m_GraphicPanel( NULL ),
  m_MotionPanel( NULL ),
  m_OgrePanel( NULL )
{

}

void MainFramePanel::loadChilds()
{
	m_ControlPanel = new ControlPanel( this );
	m_ControlPanel->loadChilds();
	m_ControlPanel->SetBackgroundColour("yellow");
	
	//m_GraphicPanel = new GraphicPanel( this );
	//m_GraphicPanel->loadChilds();
	//m_GraphicPanel->SetBackgroundColour("grey");
	m_OgrePanel = new GraphicPanelOgre( this );
	m_OgrePanel->loadChilds();
	m_OgrePanel->SetBackgroundColour("cyan");
	//SetTopWindow(m_OgrePanel);

	
	m_MotionPanel = new MotionPanel( this );
	m_MotionPanel->loadChilds();
	m_MotionPanel->SetBackgroundColour("orange");


}