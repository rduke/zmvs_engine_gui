#include "controlpanel.h"

ControlPanel::ControlPanel(wxWindow *_parent)
: wxPanel( _parent, wxID_ANY, wxPoint(0, 350), wxSize(800, 200) ),
  m_Book( NULL )
{
}

void ControlPanel::loadChilds()
{
   m_Book = new ControlBookCtrl( this );
   m_Book->loadChilds();
}

ControlPanel::~ControlPanel()
{
}