#include "motionpanel.h"

MotionPanel::MotionPanel( wxWindow* _parent )
: wxPanel( _parent, wxID_ANY, wxPoint(0, 0), wxSize(200, 350) ),
  m_MotionCombo( NULL )
{

}

void MotionPanel::loadChilds()
{
	wxString choices[] =
    {
        _T("Manual"),
        _T("Auto"),
        _T("Special auto"),
        _T("Continuous"),
    };

	m_MotionCombo = new wxComboBox( this, wxID_ANY, _T("Motions"),
                              wxPoint(20,25), wxSize(120, wxDefaultCoord),
                              5, choices,
                              wxTE_PROCESS_ENTER);
}