#include "helpmenu.h"

HelpMenu::HelpMenu()
: m_About( NULL ),
  m_Manual( NULL ),
  m_Update( NULL )
{
}

void HelpMenu::loadChilds()
{
	m_About = new wxMenuItem( this,
		                      wxID_ANY,
							  wxString( wxT("About") ) + wxT('\t') + wxT("A"),
							  wxEmptyString,
							  wxITEM_NORMAL );

	Append( m_About );

	m_Manual = new wxMenuItem( this,
		                       wxID_ANY,
							   wxString( wxT("Manual") ) + wxT('\t') + wxT("M"),
							   wxEmptyString,
							   wxITEM_NORMAL );

	Append( m_Manual );
	
	m_Update = new wxMenuItem( this,
		                       wxID_ANY,
							   wxString( wxT("Update") ) + wxT('\t') + wxT("U"),
							   wxEmptyString,
							   wxITEM_NORMAL );

	Append( m_Update );
}

